import express, { Express, Request, Response } from 'express'
import dotenv from 'dotenv'

// Configuration the .env file
dotenv.config()

// Create Express App
const app : Express = express()
const port : string | number = process.env.PORT || 8000

// Define the first app route
app.get('/', (req:Request, res:Response) => {
  // Send Hello World
  res.send('Welcome to my API Restful: Express + TS + Nodemon + Jest + Swagger + Mongoose')
})

// Define the new app route
app.get('/hello', (req:Request, res:Response) => {
  // Send Hello World
  res.send('Welcome to GET Route: Hello!')
})

// Execute APP and Listen Requests to PORT
app.listen(port, () => console.log(`Express Server running at http://localhost:${port}`))
